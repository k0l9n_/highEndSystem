package ru.itmo.highendsystem.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.itmo.highendsystem.config.CustomPostgreSQLContainer;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Testcontainers
@Disabled
public class RoleRepositoryTest extends CustomPostgreSQLContainer {

    @Autowired
    private RoleRepository roleRepository;

    @BeforeAll
    public static void setUp() {
        postgresqlContainer.start();
    }

    @AfterAll
    public static void setDown() {
        postgresqlContainer.stop();
    }


    @Test
    @Transactional
    public void findTest() {
        assertNotNull(roleRepository.findById(1L));
        assertNotNull(roleRepository.findById(2L));
        assertNotNull(roleRepository.findById(3L));
        assertNotNull(roleRepository.findById(4L));
        assertTrue(roleRepository.findById(5L).isEmpty());
    }
}
