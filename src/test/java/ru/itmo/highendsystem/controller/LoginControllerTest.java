package ru.itmo.highendsystem.controller;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.itmo.highendsystem.config.CustomPostgreSQLContainer;
import ru.itmo.highendsystem.model.dto.partial.AccountDtoForLogin;
import ru.itmo.highendsystem.model.dto.partial.DocumentDtoWithoutId;
import ru.itmo.highendsystem.model.dto.partial.HumanDtoWithoutId;
import ru.itmo.highendsystem.model.dto.partial.ShortAccountDto;
import ru.itmo.highendsystem.model.entity.DocumentType;
import ru.itmo.highendsystem.model.entity.Role;
import ru.itmo.highendsystem.repository.DocumentTypeRepository;
import ru.itmo.highendsystem.repository.RoleRepository;
import ru.itmo.highendsystem.service.business.CreateService;
import ru.itmo.highendsystem.service.business.LoginService;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@Testcontainers
@Disabled
public class LoginControllerTest extends CustomPostgreSQLContainer {
    @Autowired
    private CreateService createService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private DocumentTypeRepository documentTypeRepository;

    @BeforeAll
    public static void setUp() {
        postgresqlContainer.start();
    }

    @AfterAll
    public static void setDown() {
        postgresqlContainer.stop();
    }

    @Test
    @Transactional
    void shouldLogin() {
        roleRepository.save(new Role(1L, "test"));
        documentTypeRepository.save(new DocumentType(1L, "test"));
        var login = "testDB";
        var password = "testDB1";
        createService.createAccount(
                ShortAccountDto
                        .builder()
                        .nickname("testDB")
                        .password("testDB1")
                        .human(
                                HumanDtoWithoutId
                                        .builder()
                                        .name("AAA")
                                        .surname("AAA")
                                        .lastName("AAA")
                                        .birthDate(Date.valueOf(LocalDate.now()))
                                        .document(
                                                DocumentDtoWithoutId
                                                        .builder()
                                                        .series(10)
                                                        .number(10)
                                                        .documentTypeId(1L)
                                                        .build()
                                        )
                                        .build()
                        )
                        .registrationDate(Date.valueOf(LocalDate.now()))
                        .bonusMoney(0)
                        .roleId(1L)
                        .build()
        );

        assertNotNull(loginService.login(new AccountDtoForLogin(login, password)));
    }
}
