insert into accounts (id, nickname, password, human_id, bonus_money, registration_date, violation_count, role_id)
values (1, 'admin', '$2a$10$EV4N6aILBsKBx8VHD/cp8..YNXQdYzpL82Sj4/HhrCxg/0GKFfa2O', 1, 0,
        to_timestamp('2020-10-10', 'YYYY-MM-DD'), 0, 2);