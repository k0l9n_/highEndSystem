package ru.itmo.highendsystem.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.highendsystem.model.dto.full.FullFlightDto;
import ru.itmo.highendsystem.model.dto.partial.FlightSearchDto;
import ru.itmo.highendsystem.service.business.FlightSearchService;

import java.util.List;

/**
 * Контроллер для работы с полетами
 * */
@RestController
@RequestMapping("api/v1/airline/flights")
@RequiredArgsConstructor
public class FlightsController {

    private final FlightSearchService flightSearchService;

    @PostMapping("all")
    public ResponseEntity<List<FullFlightDto>> getFlights(@Valid @RequestBody FlightSearchDto search) {
        return ResponseEntity.ok(flightSearchService.getFlightsByFilters(search));
    }
}
