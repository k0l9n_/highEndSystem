package ru.itmo.highendsystem.advice;

import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.itmo.highendsystem.exception.base.BaseException;
import ru.itmo.highendsystem.model.dto.full.FullErrorDto;
import ru.itmo.highendsystem.model.entity.ErrorType;
import ru.itmo.highendsystem.service.data.ErrorService;
import ru.itmo.highendsystem.service.data.ErrorTypeService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Обработчки {@link BaseException}
 */
@ControllerAdvice
@RequiredArgsConstructor
public class BaseExceptionHandler extends ResponseEntityExceptionHandler {
    private final ErrorService errorService;
    private final ErrorTypeService errorTypeService;

    @ExceptionHandler(value = {BaseException.class})
    private ResponseEntity<Object> handleException(BaseException exception, WebRequest request) {
        errorService.saveError(FullErrorDto
                .builder()
                .message(exception.getDescription())
                .errorType(errorTypeService.getErrorTypeById(exception.getType().getId()))
                .build());
        return handleExceptionInternal(exception, exception.getDescription(),
                new HttpHeaders(), exception.getHttpStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        List<String> errorList = ex
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        return new ResponseEntity<>(errorList, HttpStatus.BAD_REQUEST);
    }
}
