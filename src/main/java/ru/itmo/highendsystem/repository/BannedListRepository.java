package ru.itmo.highendsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itmo.highendsystem.model.entity.BannedList;
import ru.itmo.highendsystem.model.entity.Human;

/**
 * Репозиторий сущности "Заблокированные пользователи"
 */
@Repository
public interface BannedListRepository extends JpaRepository<BannedList, Long> {

    @Query(value = "select * from banned_list b where b.human_id = :humanId", nativeQuery = true)
    BannedList findByHumanId(@Param("humanId") Long humanId);
}
