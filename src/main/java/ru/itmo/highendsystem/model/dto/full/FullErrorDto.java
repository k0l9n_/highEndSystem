package ru.itmo.highendsystem.model.dto.full;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Полное дто ошибки
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullErrorDto {
    private String message;
    private FullErrorTypeDto errorType;
}
