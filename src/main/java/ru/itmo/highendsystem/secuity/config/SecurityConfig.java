package ru.itmo.highendsystem.secuity.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.itmo.highendsystem.secuity.filter.JwtFilter;

import java.util.Map;

/**
 * Конфиг для безопасности
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private static final Map<String, String[]> patternMap = Map.of(
            "USER", new String[]{
                    "api/v1/airline/flights/all",
                    "api/v1/airline/recommendation",
                    "api/v1/airline/tickets/buy"
            },
            "ADMIN", new String[]{
                    "api/v1/airline/create/employee",
                    "api/v1/airline/create/account",
                    "api/v1/airline/violation/add",
                    "api/v1/airline/violation/ban",
                    "api/v1/airline/tickets/cost"
            },
            "EMPLOYEE", new String[]{
                    "api/v1/airline/flights/all",
                    "api/v1/airline/recommendation",
                    "api/v1/airline/tickets/buy",
            },
            "MANAGER", new String[]{
                    "api/v1/airline/crew/add",
                    "api/v1/airline/create/flight"
            }
    );


    private final JwtFilter jwtFilter;


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(
                        auth -> auth
                                .requestMatchers(patternMap.get("ADMIN")).hasAuthority("ADMIN")
                                .requestMatchers(patternMap.get("USER")).hasAuthority("USER")
                                .requestMatchers(patternMap.get("EMPLOYEE")).hasAuthority("EMPLOYEE")
                                .requestMatchers(patternMap.get("MANAGER")).hasAuthority("MANAGER")
                                .requestMatchers("api/v1/airline/login").permitAll()
                )
                .csrf(AbstractHttpConfigurer::disable)
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
