package ru.itmo.highendsystem.service.data;

import ru.itmo.highendsystem.model.dto.full.FullBannedListDto;
import ru.itmo.highendsystem.model.dto.full.FullHumanDto;

/**
 * Сервис для работы с сущностью блокировки пользователя
 */
public interface BannedListService {

    /**
     * Сохраняет сущностью блокировки пользователя
     * @param bannedListDto полное дто блокировки пользователя
     * @return true если операция завершилась успешно
     */
    Boolean saveBannedList(FullBannedListDto bannedListDto);

    /**
     * Возвращает true если данный человек забанен
     * @param humanId ИД человека
     * @return true , если есть такая запись в таблице banned_list
     */
    boolean isBan(Long humanId);
}
