package ru.itmo.highendsystem.service.data.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itmo.highendsystem.mapper.BannedListMapper;
import ru.itmo.highendsystem.model.dto.full.FullBannedListDto;
import ru.itmo.highendsystem.model.dto.full.FullHumanDto;
import ru.itmo.highendsystem.repository.BannedListRepository;
import ru.itmo.highendsystem.service.data.BannedListService;

/**
 * Реализация {@link ru.itmo.highendsystem.service.data.BannedListService}
 */
@Service
@RequiredArgsConstructor
public class BannedListServiceImpl implements BannedListService {

    private final BannedListRepository bannedListRepository;
    private final BannedListMapper bannedListMapper;

    @Override
    public Boolean saveBannedList(FullBannedListDto bannedListDto) {
        bannedListRepository.save(bannedListMapper.fullDtoToBannedList(bannedListDto));
        return true;
    }

    @Override
    public boolean isBan(Long humanId) {
        return bannedListRepository.findByHumanId(humanId) != null;
    }
}
