package ru.itmo.highendsystem.service.business.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.highendsystem.exception.buisiness.AccountAlreadyExistException;
import ru.itmo.highendsystem.exception.buisiness.CreateNotEmployeeException;
import ru.itmo.highendsystem.exception.buisiness.NotStrongPasswordException;
import ru.itmo.highendsystem.model.dto.full.*;
import ru.itmo.highendsystem.model.dto.partial.*;
import ru.itmo.highendsystem.service.business.CreateService;
import ru.itmo.highendsystem.service.data.*;

import java.util.Set;

/**
 * Реализация {@link CreateService}
 */
@Service
@RequiredArgsConstructor
public class CreateServiceImpl implements CreateService {

    private final AccountService accountService;
    private final PositionService positionService;
    private final EmployeeService employeeService;
    private final RoleService roleService;
    private final DocumentTypeService documentTypeService;
    private final PasswordEncoder passwordEncoder;
    private final FlightService flightService;
    private final FlightStatusService flightStatusService;
    private final PlaneService planeService;
    private final RouteService routeService;
    private final TicketService ticketService;

    private static final Long USER_ROLE = 1L;

    @Override
    public boolean createEmployee(ShortEmployeeDto employee) {
        FullAccountDto account = accountService.getAccountById(employee.accountId());
        FullPositionDto position = positionService.getPositionById(employee.positionId());

        Long roleId = account.getRole().getId();
        if (roleId.equals(USER_ROLE)) {
            throw new CreateNotEmployeeException();
        }
        FullEmployeeDto fullEmployeeDto = new FullEmployeeDto();
        fullEmployeeDto.setEmploymentDate(employee.employeeDate());
        fullEmployeeDto.setAccount(account);
        fullEmployeeDto.setPosition(position);
        fullEmployeeDto.setSalary(employee.salary());
        return employeeService.saveEmployee(fullEmployeeDto);
    }

    @Override
    @Transactional
    public Long createAccount(ShortAccountDto account) {
        FullRoleDto role = roleService.getRoleById(account.getRoleId());
        FullHumanDto fullHumanDto = createHuman(account.getHuman());

        if (accountService.getAccountByNickname(account.getNickname()) != null)
            throw new AccountAlreadyExistException();

        if (!checkPassword(account.getPassword()))
            throw new NotStrongPasswordException();

        FullAccountDto fullAccountDto = new FullAccountDto();
        fullAccountDto.setNickname(account.getNickname());
        fullAccountDto.setPassword(passwordEncoder.encode(account.getPassword()));
        fullAccountDto.setHuman(fullHumanDto);
        fullAccountDto.setBonusMoney(account.getBonusMoney());
        fullAccountDto.setRegistrationDate(account.getRegistrationDate());
        fullAccountDto.setViolationCount(0);
        fullAccountDto.setRole(role);

        return accountService.saveAccount(fullAccountDto).getId();
    }

    /**
     * Проверка пароля
     * @param password
     * @return
     */
    private boolean checkPassword(String password) {
        return password.matches("^(?=.*[a-zA-Z])(?=.*\\d).+$");
    }

    @Override
    public Long createFlight(ShortFlightDto flight) {
        FullFlightDto flightDto = new FullFlightDto();
        FullFlightStatusDto flightStatus = flightStatusService.getFlightStatusById(flight.flightStatus());
        FullPlaneDto plane = planeService.getPlaneById(flight.plane());
        FullRouteDto route = routeService.getRouteById(flight.route());
        flightDto.setDateStart(flight.dateStart());
        flightDto.setDateFinish(flight.dateFinish());
        flightDto.setFlightStatus(flightStatus);
        flightDto.setPlane(plane);
        flightDto.setRoute(route);

        Long id = flightService.saveFlight(flightDto).getId();

        char[] places = new char[] {'A', 'B', 'C', 'D', 'E', 'F'};
        for (int i = 1; i < plane.getPassengerCount() + 1; i++) {
            String placeName = Integer.toString(i) + places[(i - 1) % places.length];
            ShortTicketDto ticketDto = new ShortTicketDto(id, 0, placeName);
            createTicket(ticketDto);
        }

        return id;
    }

    private Long createTicket(ShortTicketDto ticket) {
        FullTicketDto ticketDto = new FullTicketDto();
        ticketDto.setFlight(flightService.getFlightById(ticket.flight()));
        ticketDto.setCost(ticket.cost());
        ticketDto.setPlace(ticket.place());

        return ticketService.saveTicket(ticketDto).getId();
    }

    /**
     * Создание человека в системе
     * @param humanDtoWithoutId краткое дто человека
     * @return полное дто человека
     */
    private FullHumanDto createHuman(HumanDtoWithoutId humanDtoWithoutId) {
        FullDocumentDto fullDocumentDto = createDocument(humanDtoWithoutId.getDocument());
        FullHumanDto fullHumanDto = new FullHumanDto();
        fullHumanDto.setName(humanDtoWithoutId.getName());
        fullHumanDto.setSurname(humanDtoWithoutId.getSurname());
        fullHumanDto.setLastName(humanDtoWithoutId.getLastName());
        fullHumanDto.setBirthDate(humanDtoWithoutId.getBirthDate());
        fullHumanDto.setDocument(fullDocumentDto);
        return fullHumanDto;
    }

    /**
     * Создание дто документа
     * @param document краткое дто документа
     * @return полное дто документа
     */
    private FullDocumentDto createDocument(DocumentDtoWithoutId document) {
        FullDocumentTypeDto documentType = documentTypeService.getDocumentTypeById(document.getDocumentTypeId());
        FullDocumentDto fullDocumentDto = new FullDocumentDto();
        fullDocumentDto.setSeries(document.getSeries());
        fullDocumentDto.setNumber(document.getNumber());
        fullDocumentDto.setDocumentType(documentType);
        return fullDocumentDto;
    }
}
