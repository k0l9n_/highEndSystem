package ru.itmo.highendsystem.service.business.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itmo.highendsystem.exception.buisiness.WrongCredentialsException;
import ru.itmo.highendsystem.model.dto.full.FullAccountDto;
import ru.itmo.highendsystem.model.dto.partial.AccountDtoForLogin;
import ru.itmo.highendsystem.secuity.util.JwtTokenUtil;
import ru.itmo.highendsystem.service.business.LoginService;
import ru.itmo.highendsystem.service.data.AccountService;
import ru.itmo.highendsystem.service.data.BannedListService;

/**
 * реализация {@link ru.itmo.highendsystem.service.business.LoginService}
 */
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenUtil jwtTokenUtil;
    private final BannedListService bannedListService;


    @Override
    public String login(AccountDtoForLogin accountDtoForLogin) {
        FullAccountDto fullAccountDto = accountService.getAccountByNickname(accountDtoForLogin.getNickname());
       if (fullAccountDto == null || !passwordEncoder.matches(accountDtoForLogin.getPassword(), fullAccountDto.getPassword()))
            throw new WrongCredentialsException(accountDtoForLogin);
       if (bannedListService.isBan(fullAccountDto.getHuman().getId()))
           throw new WrongCredentialsException(accountDtoForLogin);
        return jwtTokenUtil.generateAccessToken(accountDtoForLogin.getNickname());
    }
}
