package ru.itmo.highendsystem.service.business.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itmo.highendsystem.exception.buisiness.FlightIsNotInPreparedStatusException;
import ru.itmo.highendsystem.model.dto.full.FullCrewDto;
import ru.itmo.highendsystem.model.dto.full.FullEmployeeDto;
import ru.itmo.highendsystem.model.dto.full.FullFlightDto;
import ru.itmo.highendsystem.model.dto.partial.ShortCrewDto;
import ru.itmo.highendsystem.model.enums.FlightStatusEnum;
import ru.itmo.highendsystem.service.business.CrewManagementService;
import ru.itmo.highendsystem.service.data.CrewService;
import ru.itmo.highendsystem.service.data.EmployeeService;
import ru.itmo.highendsystem.service.data.FlightService;

/**
 * Реализация {@link CrewManagementService}
 */
@Service
@RequiredArgsConstructor
public class CrewManagementServiceImpl implements CrewManagementService {

    private final EmployeeService employeeService;
    private final FlightService flightService;
    private final CrewService crewService;


    @Override
    public Boolean addEmployeeToFlight(ShortCrewDto crewDto) {
        FullEmployeeDto fullEmployeeDto = employeeService.getById(crewDto.employeeId());
        FullFlightDto fullFlightDto = flightService.getFlightById(crewDto.flightId());

        if (!fullFlightDto.getFlightStatus().getId().equals(FlightStatusEnum.PREPARED.getId()))
            throw new FlightIsNotInPreparedStatusException();

        FullCrewDto fullCrewDto = new FullCrewDto();
        fullCrewDto.setEmployee(fullEmployeeDto);
        fullCrewDto.setFlight(fullFlightDto);
        fullCrewDto.setRole(crewDto.role());

        return crewService.saveCrew(fullCrewDto);
    }
}
