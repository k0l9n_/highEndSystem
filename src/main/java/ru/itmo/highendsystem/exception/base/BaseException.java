package ru.itmo.highendsystem.exception.base;

import org.springframework.http.HttpStatus;
import ru.itmo.highendsystem.exception.ExceptionType;

/**
 * Базовое исключение, от которого наследуются все остальные
 */
public abstract class BaseException extends RuntimeException {
    private final ExceptionType type;
    private final HttpStatus httpStatus;

    public BaseException(ExceptionType type, HttpStatus httpStatus) {
        this.type = type;
        this.httpStatus = httpStatus;
    }

    public ExceptionType getType() {
        return type;
    }

    public abstract String getDescription();

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
