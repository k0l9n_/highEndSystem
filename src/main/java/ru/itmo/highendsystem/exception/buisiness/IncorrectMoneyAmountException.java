package ru.itmo.highendsystem.exception.buisiness;

import org.springframework.http.HttpStatus;
import ru.itmo.highendsystem.exception.ExceptionType;
import ru.itmo.highendsystem.exception.base.BaseException;

/**
 * Исключение выбрасывается, когда пользователь перевёл неверную сумму для покупки билета
 */
public class IncorrectMoneyAmountException extends BaseException {
    private final Integer ticketCost;
    private final Integer userMoney;

    public IncorrectMoneyAmountException(Integer ticketCost, Integer userMoney) {
        super(ExceptionType.BUSINESS_CLIENT, HttpStatus.BAD_REQUEST);
        this.ticketCost = ticketCost;
        this.userMoney = userMoney;
    }

    public Integer getTicketCost() {
        return ticketCost;
    }

    public Integer getUserMoney() {
        return userMoney;
    }

    @Override
    public String getDescription() {
        return "Переведна неверная сумма. Переведно " + userMoney + ". Требуется " + ticketCost;
    }
}
