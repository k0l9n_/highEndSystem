package ru.itmo.highendsystem.exception.buisiness;

import org.springframework.http.HttpStatus;
import ru.itmo.highendsystem.exception.ExceptionType;
import ru.itmo.highendsystem.exception.base.BaseException;

/**
 * Исключение выбрасывается при попытке создать работника, у которого роль пользователя
 */
public class CreateNotEmployeeException extends BaseException {
    public CreateNotEmployeeException() {
        super(ExceptionType.BUSINESS_ADMIN, HttpStatus.BAD_REQUEST);
    }


    @Override
    public String getDescription() {
        return "Этот аккаунт является пользователем";
    }
}
