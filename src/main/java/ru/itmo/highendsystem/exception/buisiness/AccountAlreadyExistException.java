package ru.itmo.highendsystem.exception.buisiness;

import org.springframework.http.HttpStatus;
import ru.itmo.highendsystem.exception.ExceptionType;
import ru.itmo.highendsystem.exception.base.BaseException;

/**
 * Иключение выбрасывается при попытке создать аккаунт с ником, который уже есть
 */
public class AccountAlreadyExistException extends BaseException {
    public AccountAlreadyExistException() {
        super(ExceptionType.BUSINESS_ADMIN, HttpStatus.BAD_REQUEST);
    }

    @Override
    public String getDescription() {
        return "Такой никнейм уже используется";
    }
}
