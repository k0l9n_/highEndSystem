package ru.itmo.highendsystem.exception.buisiness;

import org.springframework.http.HttpStatus;
import ru.itmo.highendsystem.exception.ExceptionType;
import ru.itmo.highendsystem.exception.base.BaseException;

/**
 * Исключение выбрасывает, когда пароль не удавлетворяет условию
 * Условие: только латинца и обязательно цифры
 */
public class NotStrongPasswordException extends BaseException {
    public NotStrongPasswordException() {
        super(ExceptionType.BUSINESS_ADMIN, HttpStatus.BAD_REQUEST);
    }

    @Override
    public String getDescription() {
        return "Пароль должен содержать только латинские буквы и цифры";
    }
}
